# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

load("checks/buildifier/format.star", "buildifier_format")
load("checks/buildifier/lint.star", "buildifier_lint")

def register_buildifier_format(**kwargs):
    # Available arguments:
    #
    #   tool_ctx: Lambda function returning a path to buildifier host-tool
    #     Takes one parameter of type "ctx".
    #     Must exist in and be relative to the local workspace.
    #
    #   extra_args: Additional arguments passed directly to buildifier.
    #     list of strings
    #
    #   emit_level: Level for emitting messages.
    #     "notice" | "warning" | "error"
    #
    #   emit_message: Message to print in case of a found issue.
    #     String. E.g. "Please run <> to format your code."

    shac.register_check(buildifier_format.with_args(**kwargs))

def register_buildifier_lint(**kwargs):
    # Available arguments:
    #
    #   tool_ctx: Lambda function returning a path to buildifier host-tool
    #     Takes one parameter of type "ctx".
    #     Must exist in and be relative to the local workspace.
    #
    #   extra_args: Additional arguments passed directly to buildifier.
    #     list of strings
    #
    #   emit_level: Level for emitting messages.
    #     "notice" | "warning" | "error"
    #
    #   emit_message: Message to print in case of a found issue.
    #     String. E.g. "Please run <> to format your code."

    shac.register_check(buildifier_lint.with_args(**kwargs))
