# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Shac checks for formatting starlark files."""

load(
    "./common.star",
    "apply_fixes_to_files",
    "get_affected_files",
    "get_unformatted_files",
)

def _buildifier_format(
        ctx,
        tool_ctx = lambda ctx: "buildifier",
        extra_args = [],
        emit_level = "error",
        emit_message = "File is not formatted",

        # For testing
        file_filter = lambda x: True):
    """Formats Starlark/Bazel files using buildifier."""
    tool = tool_ctx(ctx)

    # Get list of affected starlark files in the workspace.
    files = get_affected_files(ctx, file_filter = file_filter)
    if not files:
        return

    # Run buildifier to identify the files that need to be formatted.
    command = [tool] + extra_args + ["--format=json", "--mode=check"]
    files_to_format = get_unformatted_files(ctx, command, files)
    if not files_to_format:
        return

    # Run buildifier on the files identified in the previous step.
    command = [tool] + extra_args
    tempfiles = apply_fixes_to_files(
        ctx,
        command,
        files_to_format,
        emit_level = emit_level,
        emit_message = emit_message,
    )

buildifier_format = shac.check(_buildifier_format, formatter = True)
