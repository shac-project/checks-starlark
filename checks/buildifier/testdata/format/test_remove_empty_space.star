# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Test verifying the buildifier formatter removes unnecessary empty space."""

def foo():
    pass


def bar():
        pass




foo()
bar()
