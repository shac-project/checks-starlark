# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Fake bazel rule, used to test the buildifier linter.

   Note: "Unused Variable" warnings cannot be automatically fixed:
   https://github.com/bazelbuild/buildtools/blob/master/WARNINGS.md#unused-variable

   There is currently no way for a "shac test" to capture the emitted output
   of a shac check, so for now this test needs to be verified manually.
"""

def foo(used_var = None, unused_var = None):
    """unused variable test."""
    bar(used_var)
