# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Fake bazel rule, used to test the buildifier linter.

   https://github.com/bazelbuild/buildtools/blob/master/WARNINGS.md#loaded-symbol-is-unused
"""

load("@foo_repo://bar.bzl", "baz")
