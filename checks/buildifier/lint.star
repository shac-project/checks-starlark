# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Shac checks for linting starlark files."""

load(
    "./common.star",
    "apply_fixes_to_files",
    "get_affected_files",
    "get_files_with_warnings",
)

def _buildifier_lint(
        ctx,
        tool_ctx = lambda ctx: "buildifier",
        extra_args = [],
        emit_level = "error",
        emit_message = "File is not linted",

        # For testing
        file_filter = lambda x: True):
    """Lints Starlark/Bazel files using buildifier."""
    tool = tool_ctx(ctx)

    # Get list of affected starlark files in the workspace.
    files = get_affected_files(ctx, file_filter = file_filter)
    if not files:
        return

    # Run buildifier in "--lint=warn" mode to identify the files that need to be linted.
    command = [tool] + extra_args + ["--format=json", "--mode=check", "--lint=warn"]
    files_to_lint = get_files_with_warnings(ctx, command, files)
    if not files_to_lint:
        return

    # Run buildifier in "--lint=fix" mode on the files identified in the previous step.
    command = [tool] + extra_args + ["--lint=fix"]
    tempfiles = apply_fixes_to_files(
        ctx,
        command,
        files_to_lint,
        emit_level = emit_level,
        emit_message = emit_message,
    )

buildifier_lint = shac.check(_buildifier_lint)
