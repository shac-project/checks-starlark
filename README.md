Shac Global Checks: Starlark Repository
=======================================

This repository hosts a suite of checks for performing static analysis
and code health operations on starlark-based filetypes.

The checks are hosted in the "checks/" directory, split up based on the
host-tool being used.

Please see https://github.com/shac-project/shac for more information on the
shac tool and framework.
